#include <argp.h>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

#include "primes.hpp"

using madlib::primes;
using std::cout;
using std::vector;

enum class primes_implementations {
  default_implementation,
  euler_sieve_mult,
  euler_sieve_sqrt
};

enum class primes_action {
  unset, // missing
  primality,
  nth,  // nth prime
  range // indexed primes in [n, m)]
};

struct cli_arguments {
  vector<size_t> num_vec;
  size_t precalculate;                               // 0
  enum primes_implementations chosen_implementation; // default
  enum primes_action action;
};

error_t arg_to_num(char *arg, size_t &num) {
  char *interval_ptr;
  errno = 0;
  num = strtoll(arg, &interval_ptr, 10);
  if (errno) {
    // TODO
    return errno;
  }
  if (*interval_ptr != '\0') {
    // Did not parse whole string, this should not happen.
    // TODO
    return -1;
  }

  return 0;
}

static error_t handle_arguments(int key, char *arg, struct argp_state *state) {

  struct cli_arguments *args =
      static_cast<struct cli_arguments *>(state->input);

  switch (key) {
  case 'a': { // Is number prime?
    if (std::string(arg) == "primality") {
      args->action = primes_action::primality;
      return 0;
    } else if ("nth") {
      args->action = primes_action::nth;
      return 0;
    } else if ("range") {
      args->action = primes_action::range;
      return 0;
    } else {
      return EINVAL;
    }
  }
  case 'c': { // Calculate primes.
    return arg_to_num(arg, args->precalculate);
  }
  case 'u': {
    // TODO magic enum
    std::string requested_implementation(arg);
    if (requested_implementation == "default_implementation") {
      args->chosen_implementation =
          primes_implementations::default_implementation;
    } else if (requested_implementation == "euler_sieve_mult") {
      args->chosen_implementation = primes_implementations::euler_sieve_mult;
    } else if (requested_implementation == "euler_sieve_sqrt") {
      args->chosen_implementation = primes_implementations::euler_sieve_sqrt;
    } else {
      return EINVAL;
    }
    return 0;
  }
  case ARGP_KEY_ARG: {
    size_t num;
    if (arg_to_num(arg, num)) {
      return EINVAL;
    }
    args->num_vec.push_back(num);
    return 0;
  }
  case ARGP_KEY_END: {
    if (args->action == primes_action::unset) {
      argp_failure(state, 1, 0, "Must specify an action.");
      return ARGP_ERR_UNKNOWN;
    } else if (args->action == primes_action::range) {
      if (args->num_vec.size() >= 2) {
        if (args->num_vec.at(args->num_vec.size() - 2) >=
            args->num_vec.back()) {
          argp_failure(state, 1, 0,
                       "Last two numbers given must satisfy `n<m`.");
          return EINVAL;
        } else {
          return 0;
        }
      } else {
        argp_failure(state, 1, 0, "Range action requires two numbers.");
        return EINVAL;
      }
    } else {
      return 0;
    }
  }
  default: {
    return ARGP_ERR_UNKNOWN;
  }
  }
}

const struct argp_option options[] = {
    {"action", 'a', "N", 0,
     "Direct which action to perform from primality, nth, and range.  "
     "'primality' prints 'true' if prime else 'false' each bare number passed. "
     " 'nth' print the nth prime (0-indexed) for each bare number passed.  "
     "'range' prints the nth..(mth-1) primes using 0-indexing for the last two "
     "numbers passed; last two numbers must satisfy `n<m`.",
     0},
    {"precalculate", 'c', "N", 0,
     "Precalculate the primality of all numbers less than or equal to the "
     "passed number.",
     0},
    {"use", 'u', "NAME", 0,
     "Specifies which backend implementation from 'euler_sieve_mult', and "
     "'euler_sieve_sqrt' to use when given.",
     0},
    {nullptr, 0, nullptr, 0, nullptr, 0}};

struct argp supported_arguments = {
    options, handle_arguments, nullptr,
    "Basic utility for finding and interacting with primes.", nullptr,

    /* If non-zero, this should be a function to filter the output of help
       messages.  KEY is either a key from an option, in which case TEXT is
       that option's help text, or a special key from the ARGP_KEY_HELP_
       defines, below, describing which other help text TEXT is.  The function
       should return either TEXT, if it should be used as-is, a replacement
       string, which should be malloced, and will be freed by argp, or NULL,
       meaning `print nothing'.  The value for TEXT is *after* any translation
       has been done, so if any of the replacement text also needs translation,
       that should be done by the filter function.  INPUT is either the input
       supplied to argp_parse, or NULL, if argp_help was called directly.  */
    // char *(*help_filter) (int __key, const char *__text, void *__input) =
    // nullptr;
    nullptr,

    /* If non-zero the strings used in the argp library are translated using
       the domain described by this string.  Otherwise the currently installed
       default domain is used.  */
    // const char *argp_domain = nullptr;
    nullptr};

template <typename T>
void perform_requested_actions(primes<T> primes_object,
                               const cli_arguments &parsed_args) {
  if (parsed_args.action == primes_action::primality) {
    for (auto num : parsed_args.num_vec) {
      printf(primes_object.is_prime(num) ? "true " : "false ");
    }
    printf("\n");
  } else if (parsed_args.action == primes_action::nth) {
    for (auto num : parsed_args.num_vec) {
      printf("%lu ", primes_object.get_nth_prime(num));
    }
    printf("\n");
  } else if (parsed_args.action == primes_action::range) {
    for (auto idx = parsed_args.num_vec.at(parsed_args.num_vec.size() - 2);
         idx < parsed_args.num_vec.back(); ++idx) {
      printf("%lu ", primes_object.get_nth_prime(idx));
    }
  }
}

int main(int argc, char **argv) {

  int arg_index;
  cli_arguments parsed_args;
  parsed_args.action = primes_action::unset;
  parsed_args.precalculate = 0;
  parsed_args.chosen_implementation =
      primes_implementations::default_implementation;
  argp_program_version = "primes @GIT_VER_REGEX_REPLACE@";
  argp_program_bug_address = "<joshua.r.marshall.1991@gmail.com>";

  if (error_t error = argp_parse(&supported_arguments, argc, argv, 0,
                                 &arg_index, &parsed_args)) {
    return error;
  }

  if (parsed_args.chosen_implementation ==
      primes_implementations::default_implementation) {
    perform_requested_actions(primes(parsed_args.precalculate), parsed_args);
  } else if (parsed_args.chosen_implementation ==
             primes_implementations::euler_sieve_mult) {
    perform_requested_actions(
        primes<euler_sieve_mult>(parsed_args.precalculate), parsed_args);
  } else if (parsed_args.chosen_implementation ==
             primes_implementations::euler_sieve_sqrt) {
    perform_requested_actions(
        primes<euler_sieve_sqrt>(parsed_args.precalculate), parsed_args);
  } else {
    return -1; // Should never reach here.
  }

  return 0;
}
