#include <cstddef>

#include "primes.h"
#include "primes.hpp"

/// This is the C compatability interface.  It offers similar functionality as
/// the C++ interface but the user cannot select a preferred implementation.

extern "C" {
/// C wrapper around the C++ primes implementation.  This can only use the
/// default primes algorithm implementation.  It keeps track of any sort of
/// memory management and current prime.
struct c_primes {
  madlib::primes<> primes_object;
  size_t curr_idx;
};

/// Wrap construction of C++ primes object.
struct c_primes *
construct_c_primes_object(const unsigned long long int precalculate_through_n) {
  auto *to_return = new struct c_primes;
  to_return->primes_object.calculate_primes_through(precalculate_through_n);
  to_return->curr_idx = 0;

  return to_return;
}

/// Wrap deconstruction of C++ primes object.
void destruct_c_primes_object(struct c_primes *primes) { delete primes; }

/// Calculate ahead of time all primes less than `n`.  Using this often
/// allows faster operation of the backend implementation.
struct c_primes *precalculate_primes_through(struct c_primes *primes,
                                             const unsigned long long int n) {
  primes->primes_object.calculate_primes_through(n);

  return primes;
}

/// Get the nth prime.  Will calculate needed primes if needed.
unsigned long long int get_nth_prime(struct c_primes *primes,
                                     const unsigned long long int n) {
  return primes->primes_object.get_nth_prime(n);
}

/// Increment the current prime `primes` is referring to.
struct c_primes *increment_current_prime(struct c_primes *primes) {
  primes->curr_idx++;

  return primes;
}

/// Decrement the current prime `primes` is referring to.
struct c_primes *decrement_current_prime(struct c_primes *primes) {
  primes->curr_idx--;

  return primes;
}

/// Change the current prime `primes` refers to relative to the current
/// prime `primes` is referring to.
struct c_primes *
relative_change_current_primes(struct c_primes *primes,
                               const long long int rel_offset) {
  primes->curr_idx =
      static_cast<size_t>(static_cast<ssize_t>(primes->curr_idx) + rel_offset);

  return primes;
}

/// Change the current prime `primes` refers to according to the absolute
/// indexing of primes.
struct c_primes *
absolute_change_current_primes(struct c_primes *primes,
                               const unsigned long long int index) {
  primes->curr_idx = index;

  return primes;
}

/// Retern the current prime `primes` is referring to.
unsigned long long int get_current_prime(struct c_primes *primes) {
  return static_cast<unsigned long long int>(
      primes->primes_object.get_nth_prime(primes->curr_idx));
}

/// Tell if `n` is prime or not.
bool is_prime(struct c_primes *primes, const unsigned long long int n) {
  return primes->primes_object.is_prime(n);
}
};
