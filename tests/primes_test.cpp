#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest/doctest.h>

#include "primes.hpp"

using namespace std;
using madlib::primes;
using madlib::primes_iterator;

TEST_CASE("Iterator value_type is size_t") {
  CHECK(std::is_same_v<primes_iterator<>::value_type, size_t>);
}

TEST_CASE("Iterator difference_type is size_t") {
  CHECK(std::is_same_v<primes_iterator<>::difference_type, ssize_t>);
}

TEST_CASE("Primes value_type is size_t") {
  CHECK(std::is_same_v<primes<>::value_type, size_t>);
}

TEST_CASE("Primes iterator is primes_iterator") {
  CHECK(std::is_same_v<primes<>::iterator,
                       primes_iterator<>>);
}

TEST_CASE("Primes const_iterator is const primes_iterator<>") {
  CHECK(std::is_same_v<primes<>::const_iterator,
                       const primes_iterator<>>);
}

TEST_CASE("Primes difference_type is size_t") {
  CHECK(std::is_same_v<primes<>::difference_type, ssize_t>);
}

TEST_CASE("Primes object constructs and deconstructs") {
  madlib::primes exists;
}

TEST_CASE("Primes builds primes through 20") {
  madlib::primes primes_object(static_cast<int>(20));
}

TEST_CASE("Primes constructor at 0") {
  madlib::primes primes_object(static_cast<int>(0));
}

TEST_CASE("Primes constructor at unsigned 0") {
  madlib::primes primes_object(static_cast<size_t>(0));
}

TEST_CASE("Primes constructor at negative 0") {
  madlib::primes primes_object(-1);
}

TEST_CASE("Primes builds primes through unsigned integer 20") {
  madlib::primes primes_object(static_cast<size_t>(20));
}

TEST_CASE("Primes precalculates correct primes through 20") {
  madlib::primes primes_object(20);

  CHECK(primes_object.is_prime(0) == false);
  CHECK(primes_object.is_prime(1) == false);
  CHECK(primes_object.is_prime(2) == true);
  CHECK(primes_object.is_prime(3) == true);
  CHECK(primes_object.is_prime(4) == false);
  CHECK(primes_object.is_prime(5) == true);
  CHECK(primes_object.is_prime(6) == false);
  CHECK(primes_object.is_prime(7) == true);
  CHECK(primes_object.is_prime(8) == false);
  CHECK(primes_object.is_prime(9) == false);
  CHECK(primes_object.is_prime(10) == false);
  CHECK(primes_object.is_prime(11) == true);
  CHECK(primes_object.is_prime(12) == false);
  CHECK(primes_object.is_prime(13) == true);
  CHECK(primes_object.is_prime(14) == false);
  CHECK(primes_object.is_prime(15) == false);
  CHECK(primes_object.is_prime(16) == false);
  CHECK(primes_object.is_prime(17) == true);
  CHECK(primes_object.is_prime(18) == false);
  CHECK(primes_object.is_prime(19) == true);
}

TEST_CASE("Primes incrementally calculates correct primes through 20") {
  madlib::primes primes_object;

  CHECK(primes_object.is_prime(0) == false);
  CHECK(primes_object.is_prime(1) == false);
  CHECK(primes_object.is_prime(2) == true);
  CHECK(primes_object.is_prime(3) == true);
  CHECK(primes_object.is_prime(4) == false);
  CHECK(primes_object.is_prime(5) == true);
  CHECK(primes_object.is_prime(6) == false);
  CHECK(primes_object.is_prime(7) == true);
  CHECK(primes_object.is_prime(8) == false);
  CHECK(primes_object.is_prime(9) == false);
  CHECK(primes_object.is_prime(10) == false);
  CHECK(primes_object.is_prime(11) == true);
  CHECK(primes_object.is_prime(12) == false);
  CHECK(primes_object.is_prime(13) == true);
  CHECK(primes_object.is_prime(14) == false);
  CHECK(primes_object.is_prime(15) == false);
  CHECK(primes_object.is_prime(16) == false);
  CHECK(primes_object.is_prime(17) == true);
  CHECK(primes_object.is_prime(18) == false);
  CHECK(primes_object.is_prime(19) == true);
}

TEST_CASE("Test small primes using unsigned integers.") {
  madlib::primes primes_object;

  size_t i = 0;

  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == true);
  CHECK(primes_object.is_prime(i++) == true);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == true);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == true);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == true);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == true);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == true);
  CHECK(primes_object.is_prime(i++) == false);
  CHECK(primes_object.is_prime(i++) == true);
}

TEST_CASE("Primes iterator can access primes through 20") {
  madlib::primes primes_object;

  vector<size_t> recorded_primes;
  size_t count = 0;
  for (auto itr = primes_object.begin(); count < 8; ++count) {
    recorded_primes.push_back(*(itr++));
  }
}

TEST_CASE("Primes iterator has correct primes through 20") {
  madlib::primes primes_object;

  vector<size_t> recorded_primes;
  size_t count = 0;
  for (auto itr = primes_object.begin(); count < 8; ++count) {
    recorded_primes.push_back(*(itr++));
  }

  CHECK(recorded_primes[0] == 2);
  CHECK(recorded_primes[1] == 3);
  CHECK(recorded_primes[2] == 5);
  CHECK(recorded_primes[3] == 7);
  CHECK(recorded_primes[4] == 11);
  CHECK(recorded_primes[5] == 13);
  CHECK(recorded_primes[6] == 17);
  CHECK(recorded_primes[7] == 19);
}

TEST_CASE("test_iterator_equality_true") {
  primes primes_object;
  auto left = primes_object.begin();
  auto right = primes_object.begin();

  CHECK(left == right);
}

TEST_CASE("test_iterator_equality_false") {
  primes primes_object;
  auto left = primes_object.begin();
  auto right = primes_object.begin();
  right++;

  CHECK((left == right) == false);
}

TEST_CASE("test_iterator_inequality_true") {
  primes primes_object;
  auto left = primes_object.begin();
  auto right = primes_object.begin();
  right++;

  CHECK(left != right);
}

TEST_CASE("test_iterator_inequality_false") {
  primes primes_object;
  auto left = primes_object.begin();
  auto right = primes_object.begin();

  CHECK((left != right) == false);
}

TEST_CASE("Calculate through single unsigned 0 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<unsigned int>(0));
}

TEST_CASE("Calculate through single signed 0 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<int>(0));
}

TEST_CASE("Calculate through double unsigned 0 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<unsigned int>(0));
  primes_object.calculate_primes_through(static_cast<unsigned int>(0));
}

TEST_CASE("Calculate through double signed 1 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<int>(1));
  primes_object.calculate_primes_through(static_cast<int>(1));
}

TEST_CASE("Calculate through single unsigned 1 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<unsigned int>(1));
}

TEST_CASE("Calculate through single signed 1 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<int>(1));
}

TEST_CASE("Calculate through double unsigned 1 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<unsigned int>(1));
  primes_object.calculate_primes_through(static_cast<unsigned int>(1));
}

TEST_CASE("Calculate through double signed 1 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<int>(1));
  primes_object.calculate_primes_through(static_cast<int>(1));
}

TEST_CASE("Calculate through single signed -1 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<int>(-1));
}

TEST_CASE("Calculate through double signed -1 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<int>(-1));
  primes_object.calculate_primes_through(static_cast<int>(-1));
}

TEST_CASE("Calculate through single unsigned 20 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<unsigned int>(20));
}

TEST_CASE("Calculate through single signed 20 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<int>(20));
}

TEST_CASE("Calculate through double unsigned 20 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<unsigned int>(20));
  primes_object.calculate_primes_through(static_cast<unsigned int>(20));
}

TEST_CASE("Calculate through double signed 20 case") {
  primes primes_object;

  primes_object.calculate_primes_through(static_cast<int>(20));
  primes_object.calculate_primes_through(static_cast<int>(20));
}
