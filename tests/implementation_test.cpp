#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest/doctest.h>

#define Q(x) #x
#define QUOTE(X) Q(X)

#include QUOTE(TEST_PRIMES_IMPLEMENTATION_PATH)

using namespace std;
// using madlib::euler_sieve_mult;

TEST_CASE("Primes object constructs and deconstructs") { typename TEST_PRIMES_IMPLEMENTATION exists; }

TEST_CASE("Primes constructor at unsigned 0") {
  typename TEST_PRIMES_IMPLEMENTATION primes_object(static_cast<size_t>(0));
}

TEST_CASE("Primes builds primes through unsigned integer 20") {
  typename TEST_PRIMES_IMPLEMENTATION primes_object(static_cast<size_t>(20));
}

TEST_CASE("Primes precalculates correct primes through 20") {
  typename TEST_PRIMES_IMPLEMENTATION primes_object(static_cast<size_t>(20));

  size_t i = 0;

  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == true);
  CHECK(primes_object.is_prime_impl(i++) == true);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == true);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == true);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == true);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == true);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == true);
  CHECK(primes_object.is_prime_impl(i++) == false);
  CHECK(primes_object.is_prime_impl(i++) == true);
}

TEST_CASE("Primes incrementally calculates correct primes through 20") {
  typename TEST_PRIMES_IMPLEMENTATION primes_object;

  size_t i = 0;

  CHECK(primes_object.is_prime_impl(i++) == false); // 0
  CHECK(primes_object.is_prime_impl(i++) == false); // 1
  CHECK(primes_object.is_prime_impl(i++) == true);  // 2
  CHECK(primes_object.is_prime_impl(i++) == true);  // 3
  CHECK(primes_object.is_prime_impl(i++) == false); // 4
  CHECK(primes_object.is_prime_impl(i++) == true);  // 5
  CHECK(primes_object.is_prime_impl(i++) == false); // 6
  CHECK(primes_object.is_prime_impl(i++) == true);  // 7
  CHECK(primes_object.is_prime_impl(i++) == false); // 8
  CHECK(primes_object.is_prime_impl(i++) == false); // 9
  CHECK(primes_object.is_prime_impl(i++) == false); // 10
  CHECK(primes_object.is_prime_impl(i++) == true);  // 11
  CHECK(primes_object.is_prime_impl(i++) == false); // 12
  CHECK(primes_object.is_prime_impl(i++) == true);  // 13
  CHECK(primes_object.is_prime_impl(i++) == false); // 14
  CHECK(primes_object.is_prime_impl(i++) == false); // 15
  CHECK(primes_object.is_prime_impl(i++) == false); // 16
  CHECK(primes_object.is_prime_impl(i++) == true);  // 17
  CHECK(primes_object.is_prime_impl(i++) == false); // 18
  CHECK(primes_object.is_prime_impl(i++) == true);  // 19
}

TEST_CASE("Calculate through single unsigned 0 case") {
  typename TEST_PRIMES_IMPLEMENTATION primes_object;

  primes_object.calculate_primes_through_impl(static_cast<unsigned int>(0));
}

TEST_CASE("Calculate through double unsigned 0 case") {
  typename TEST_PRIMES_IMPLEMENTATION primes_object;

  primes_object.calculate_primes_through_impl(static_cast<unsigned int>(0));
  primes_object.calculate_primes_through_impl(static_cast<unsigned int>(0));
}

TEST_CASE("Calculate through single unsigned 1 case") {
  typename TEST_PRIMES_IMPLEMENTATION primes_object;

  primes_object.calculate_primes_through_impl(static_cast<unsigned int>(1));
}

TEST_CASE("Calculate through double unsigned 1 case") {
  typename TEST_PRIMES_IMPLEMENTATION primes_object;

  primes_object.calculate_primes_through_impl(static_cast<unsigned int>(1));
  primes_object.calculate_primes_through_impl(static_cast<unsigned int>(1));
}

TEST_CASE("Calculate through single unsigned 20 case") {
  typename TEST_PRIMES_IMPLEMENTATION primes_object;

  primes_object.calculate_primes_through_impl(static_cast<unsigned int>(20));
}

TEST_CASE("Calculate through double unsigned 20 case") {
  typename TEST_PRIMES_IMPLEMENTATION primes_object;

  primes_object.calculate_primes_through_impl(static_cast<unsigned int>(20));
  primes_object.calculate_primes_through_impl(static_cast<unsigned int>(20));
}

