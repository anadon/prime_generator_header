#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "primes.hpp"

using namespace std;

using madlib::prime_generator;
using madlib::primes;

void test_incremental() {
  madlib::primes primes_object;

  cout << 0 << ":"
       << ((primes_object.is_prime(0) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 1 << ":"
       << ((primes_object.is_prime(1) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 2 << ":"
       << ((primes_object.is_prime(2) == true) ? string("true")
                                               : string("false"))
       << endl;
  cout << 3 << ":"
       << ((primes_object.is_prime(3) == true) ? string("true")
                                               : string("false"))
       << endl;
  cout << 4 << ":"
       << ((primes_object.is_prime(4) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 5 << ":"
       << ((primes_object.is_prime(5) == true) ? string("true")
                                               : string("false"))
       << endl;
  cout << 6 << ":"
       << ((primes_object.is_prime(6) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 7 << ":"
       << ((primes_object.is_prime(7) == true) ? string("true")
                                               : string("false"))
       << endl;
  cout << 8 << ":"
       << ((primes_object.is_prime(8) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 9 << ":"
       << ((primes_object.is_prime(9) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 10 << ":"
       << ((primes_object.is_prime(10) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 11 << ":"
       << ((primes_object.is_prime(11) == true) ? string("true")
                                                : string("false"))
       << endl;
  cout << 12 << ":"
       << ((primes_object.is_prime(12) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 13 << ":"
       << ((primes_object.is_prime(13) == true) ? string("true")
                                                : string("false"))
       << endl;
  cout << 14 << ":"
       << ((primes_object.is_prime(14) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 15 << ":"
       << ((primes_object.is_prime(15) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 16 << ":"
       << ((primes_object.is_prime(16) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 17 << ":"
       << ((primes_object.is_prime(17) == true) ? string("true")
                                                : string("false"))
       << endl;
  cout << 18 << ":"
       << ((primes_object.is_prime(18) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 19 << ":"
       << ((primes_object.is_prime(19) == true) ? string("true")
                                                : string("false"))
       << endl;
}

void test_bulk() {
  madlib::primes primes_object(20);

  cout << 0 << ":"
       << ((primes_object.is_prime(0) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 1 << ":"
       << ((primes_object.is_prime(1) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 2 << ":"
       << ((primes_object.is_prime(2) == true) ? string("true")
                                               : string("false"))
       << endl;
  cout << 3 << ":"
       << ((primes_object.is_prime(3) == true) ? string("true")
                                               : string("false"))
       << endl;
  cout << 4 << ":"
       << ((primes_object.is_prime(4) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 5 << ":"
       << ((primes_object.is_prime(5) == true) ? string("true")
                                               : string("false"))
       << endl;
  cout << 6 << ":"
       << ((primes_object.is_prime(6) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 7 << ":"
       << ((primes_object.is_prime(7) == true) ? string("true")
                                               : string("false"))
       << endl;
  cout << 8 << ":"
       << ((primes_object.is_prime(8) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 9 << ":"
       << ((primes_object.is_prime(9) == false) ? string("true")
                                                : string("false"))
       << endl;
  cout << 10 << ":"
       << ((primes_object.is_prime(10) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 11 << ":"
       << ((primes_object.is_prime(11) == true) ? string("true")
                                                : string("false"))
       << endl;
  cout << 12 << ":"
       << ((primes_object.is_prime(12) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 13 << ":"
       << ((primes_object.is_prime(13) == true) ? string("true")
                                                : string("false"))
       << endl;
  cout << 14 << ":"
       << ((primes_object.is_prime(14) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 15 << ":"
       << ((primes_object.is_prime(15) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 16 << ":"
       << ((primes_object.is_prime(16) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 17 << ":"
       << ((primes_object.is_prime(17) == true) ? string("true")
                                                : string("false"))
       << endl;
  cout << 18 << ":"
       << ((primes_object.is_prime(18) == false) ? string("true")
                                                 : string("false"))
       << endl;
  cout << 19 << ":"
       << ((primes_object.is_prime(19) == true) ? string("true")
                                                : string("false"))
       << endl;
}

void test_iterator() {
  madlib::primes primes_object;

  vector<size_t> recorded_primes;
  size_t count = 0;
  for (auto itr = primes_object.begin(); count < 8; ++count) {
    recorded_primes.push_back(*(itr++));
  }

  cout << "Reported primes from iterator are: ";
  for (auto num : recorded_primes) {
    cout << num << ", ";
  }
  cout << "\n";
}

void direct_use_example() {
  primes primes_object(5);

  primes_object.calculate_primes_through(static_cast<unsigned int>(11));

  cout << "Primes: ";
  for (size_t idx = 0; primes_object.get_nth_prime(idx) < 5; ++idx) {
    cout << primes_object.get_nth_prime(idx);
  }
  cout << endl;

  for (size_t i = 0; i <= 5; ++i) {
    if (primes_object.is_prime(i)) {
      cout << i << " is prime." << endl;
    } else {
      cout << i << " is not prime." << endl;
    }
  }
}

void iterator_use_example() {
  primes primes_object(10);

  for (auto itr = primes_object.begin();
       distance(primes_object.begin(), itr) < 5; ++itr)
    *itr;
}

void iterator_infinite_loop_example() {
  primes primes_object;

  int breaker = 0;
  for (auto itr = primes_object.begin(); itr != primes_object.end(); ++itr) {
    *itr;

    if (breaker++ >= 5)
      break;
  }
}

void generator_example() {
  array<size_t, 5> arr;

  generate(arr.begin(), arr.end(), prime_generator());
}

void generator_example_2() {
  auto gen = prime_generator();
  gen();
  gen();
  gen();
  gen();
  gen();
  gen();
}

int main() {
  test_incremental();
  test_bulk();
  test_iterator();
  generator_example();
  iterator_infinite_loop_example();
  iterator_use_example();
  direct_use_example();
  generator_example_2();

  return 0;
}
