#include <assert.h>
#include <stddef.h>

#include "primes.h"

void primes_object_constructs_and_deconstructs() {
  struct c_primes *exists = construct_c_primes_object(0);
  assert(exists != NULL);
  destruct_c_primes_object(exists);
}

void primes_builds_primes_through_20() {
  struct c_primes *primes_object = construct_c_primes_object(20);
  assert(primes_object != NULL);
  destruct_c_primes_object(primes_object);
}

void primes_precalculates_correct_primes_through_20() {
  struct c_primes *primes_object = construct_c_primes_object(20);

  assert(is_prime(primes_object, 0) == false);
  assert(is_prime(primes_object, 1) == false);
  assert(is_prime(primes_object, 2) == true);
  assert(is_prime(primes_object, 3) == true);
  assert(is_prime(primes_object, 4) == false);
  assert(is_prime(primes_object, 5) == true);
  assert(is_prime(primes_object, 6) == false);
  assert(is_prime(primes_object, 7) == true);
  assert(is_prime(primes_object, 8) == false);
  assert(is_prime(primes_object, 9) == false);
  assert(is_prime(primes_object, 10) == false);
  assert(is_prime(primes_object, 11) == true);
  assert(is_prime(primes_object, 12) == false);
  assert(is_prime(primes_object, 13) == true);
  assert(is_prime(primes_object, 14) == false);
  assert(is_prime(primes_object, 15) == false);
  assert(is_prime(primes_object, 16) == false);
  assert(is_prime(primes_object, 17) == true);
  assert(is_prime(primes_object, 18) == false);
  assert(is_prime(primes_object, 19) == true);

  destruct_c_primes_object(primes_object);
}

void primes_incrementally_calculates_correct_primes_through_20() {
  struct c_primes *primes_object = construct_c_primes_object(0);

  assert(is_prime(primes_object, 0) == false);
  assert(is_prime(primes_object, 1) == false);
  assert(is_prime(primes_object, 2) == true);
  assert(is_prime(primes_object, 3) == true);
  assert(is_prime(primes_object, 4) == false);
  assert(is_prime(primes_object, 5) == true);
  assert(is_prime(primes_object, 6) == false);
  assert(is_prime(primes_object, 7) == true);
  assert(is_prime(primes_object, 8) == false);
  assert(is_prime(primes_object, 9) == false);
  assert(is_prime(primes_object, 10) == false);
  assert(is_prime(primes_object, 11) == true);
  assert(is_prime(primes_object, 12) == false);
  assert(is_prime(primes_object, 13) == true);
  assert(is_prime(primes_object, 14) == false);
  assert(is_prime(primes_object, 15) == false);
  assert(is_prime(primes_object, 16) == false);
  assert(is_prime(primes_object, 17) == true);
  assert(is_prime(primes_object, 18) == false);
  assert(is_prime(primes_object, 19) == true);

  destruct_c_primes_object(primes_object);
}

void primes_iterator_can_access_primes_through_20() {
  struct c_primes *primes_object = construct_c_primes_object(0);

  for (int cnt = 0; cnt < 20; ++cnt) {
    get_current_prime(primes_object);
    increment_current_prime(primes_object);
  }

  destruct_c_primes_object(primes_object);
}

void primes_iterator_has_correct_primes_through_20() {
  struct c_primes *primes_object = construct_c_primes_object(0);

  assert(get_current_prime(primes_object) == 2);
  assert(get_current_prime(increment_current_prime(primes_object)) == 3);
  assert(get_current_prime(increment_current_prime(primes_object)) == 5);
  assert(get_current_prime(increment_current_prime(primes_object)) == 7);
  assert(get_current_prime(increment_current_prime(primes_object)) == 11);
  assert(get_current_prime(increment_current_prime(primes_object)) == 13);
  assert(get_current_prime(increment_current_prime(primes_object)) == 17);
  assert(get_current_prime(increment_current_prime(primes_object)) == 19);

  destruct_c_primes_object(primes_object);
}

int main() {
  primes_object_constructs_and_deconstructs();
  primes_builds_primes_through_20();
  primes_precalculates_correct_primes_through_20();
  primes_incrementally_calculates_correct_primes_through_20();
  primes_iterator_can_access_primes_through_20();
  primes_iterator_has_correct_primes_through_20();

  return 0;
}
