# Prime Header's documentation!

# Caution: This is verion 0 and rapidly developing.  More tests are always being added, but expect breakages until version 1.

[![OpenSSF Best Practices](https://bestpractices.coreinfrastructure.org/projects/6617/badge)](https://bestpractices.coreinfrastructure.org/projects/6617)[![Build Status](https://app.travis-ci.com/anadon/prime_generator_header.svg?branch=master)](https://app.travis-ci.com/gitlab/anadon/prime_generator_header)[![Build status](https://ci.appveyor.com/api/projects/status/l0xca2glaewwdarx/branch/master?svg=true)](https://ci.appveyor.com/project/anadon/prime-generator-header/branch/master)[![Documentation Status](https://readthedocs.org/projects/prime-generator-header/badge/?version=latest)](https://prime-generator-header.readthedocs.io/en/latest/?badge=latest)

[For project documentation, click here](https://prime-generator-header.readthedocs.io/en/latest/)

## What 'Prime Header' Does

This project is a C++20 constexpr header-only library.  It has been developed to provide a reasonably complete set of mechanisms to get and use primes.  Development is in progress to make it an excellent reference project for

- Code documentation
- Making documentation accessible in multiple forms
- Packaging
- Examples
- Test suite
- CI support

And anything else to make this project the best it can possibly be in hopes of making a reference and standard from which other project (mine at least) can copy from and be held to.

## Installing

### Dependencies

Dependencies only need to be installed from one source.  However, many systems provide them from multiple sources.  Depending on your system, one source may be better than another.

#### System Only Dependencies:
- ninja
- G++>=12
- Python>=3.7
- Doxygen
- doctest-dev
- pkgconfig

#### Python Dependencies or System Dependencies
- meson
- sphinx
- breathe
- exhale
- compile-time-printer

Install on Ubuntu, Debian, Pop_OS:
```
   sudo apt install ninja-build g++-12 doxygen python3 pkgconf doctest-dev
   pip install -r docs/requirements.txt
```

Testing:
```
   meson setup build
   meson test -C  build
```

Installing:
```
   meson setup build
   meson install -C build
```

Details:
At time of writing, the C++ Standards Committee has not approved P1929r0, which allows for some important components to be made `constexpr`.  The only compiler tested to work with this is GCC 12.  It is expected that the needed conponents will be made `constexpr` in C++23.  This code requires a C++20 compiler which has also adopted an implementation conforming to P1929r0.  The Python module `compile-time-printer` also requires GCC, further limiting compiler support.  This situation will improve in the future.


### Building

```
   meson build
   cd build
   ninja
```

### Installing

```
   cd build
   meson install
```

This project has not been packaged yet, so it is best to just download the header in `include/` and place it whereever useful.

## Examples

Include and Use:
```
   #include <primes.hpp>

   using madlib::primes;
   using madlib::prime_generator;
```

Suggested Direct Use
```
   void direct_use_example(){
     primes primes_object(5);

     primes_object.calculate_primes_through(static_cast<unsigned int>(11));

     cout << "Primes: "
     for(size_t idx = 0; primes_object.get_nth_prime(idx) < 5; ++idx){
       cout << primes_object.get_nth_prime(idx);
     }
     cout << endl;

     for(size_t i = 0; i <= 5; ++i){
       if(primes_object.is_prime(i)){
         cout << i << " is prime." << endl;
       }else{
         cout << i << " is not prime." << endl;
       }
     }
   }
```

```
   Primes: 2 3 5 7 11
   0 is not prime.
   1 is not prime.
   2 is prime.
   3 is prime.
   4 is not prime.
   5 is prime.
```

Since primes don't entirely act like a typical finite container like most C++ data structures, loops should be thought of in a more guarded way, as shown below.

Suggested Iterator Loop
```
   void iterator_use_example(){
     primes primes_object(10);

     cout << "Primes: "
     for(auto itr = primes_object.begin(); distance(primes_object.begin(), itr) < 5; ++itr)
       cout << *itr << endl;
   }
```

```
   Primes: 2 3 5 7 11
```

Be careful, as this is actually an infinite loop unless guarded.

Typical Iterator Loop
```
   void iterator_infinite_loop_example(){
     primes primes_object;

     cout << "Primes: ";
     int breaker = 0;
     for(auto itr = primes_object.begin(); itr != primes_object.end(); ++itr){
        << *itr << " ";

       if(breaker++ >= 5)
         break;
     }
   }
```

```
   Primes: 2 3 5 7 11
```


Use wisely, this may overcalculate primes to the detriment of speed and memory usage.

Generator Pattern
```
   void generator_example(){
     array<primes::value_type, 5> arr;

     generate(arr.begin(), arr.end(), prime_generator());

     cout << "Primes: "
     for(auto val : arr)
       cout << val << " ";
   }
```

```
   Primes: 2 3 5 7 11
```


## Work needed for this project

This project has not reached version 1.  It needs significant work in order to reach this mile stone.  For more details, please see [here for planning](https://prime-generator-header.readthedocs.io/en/latest/planning.html).
