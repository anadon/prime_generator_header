#include <algorithm>
#include <array>
#include <cmath>
#include <ctp/ctp.hpp>
#include <vector>

#include "primes.hpp"

using namespace std;
using ctp::printf;
using madlib::primes;
using madlib::prime_generator;


constexpr void direct_use_example(){
  primes primes_object(5);

  primes_object.calculate_primes_through(static_cast<unsigned int>(11));

  printf("Primes: ");
  for(size_t idx = 0; primes_object.get_nth_prime(idx) < 5; ++idx){
    printf("{} ", primes_object.get_nth_prime(idx));
  }
  printf("\n");

  for(size_t i = 0; i <= 5; ++i){
    if(primes_object.is_prime(i)){
      printf("{} is prime.\n", i);
    }else{
      printf("{} is not prime.\n", i);
    }
  }
}

constexpr void iterator_use_example(){
  primes primes_object(10);

  printf("Primes: ");
  for(auto itr = primes_object.begin(); distance(primes_object.begin(), itr) < 5; ++itr)
    printf("{} ", *itr);
}

void iterator_infinite_loop_example(){
  primes primes_object;

  printf("Primes: ");
  int breaker = 0;
  for(auto itr = primes_object.begin(); itr != primes_object.end(); ++itr){
    printf("{} ", *itr);

    if(breaker++ >= 5)
      break;
  }
}

constexpr void generator_example(){
  array<primes::value_type, 5> arr;

  generate(arr.begin(), arr.end(), prime_generator());

  printf("Primes: ");
  for(auto val : arr)
    printf("{} ", val);
}

int main(){
  direct_use_example();
  iterator_use_example();
  iterator_infinite_loop_example();
  generator_example();

  return 0;
}