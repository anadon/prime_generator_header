#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <vector>

#include "primes.hpp"

using namespace std;
using madlib::primes;
using madlib::prime_generator;


void direct_use_example(){
  primes primes_object(5);

  primes_object.calculate_primes_through(static_cast<unsigned int>(11));

  cout << "Primes: ";
  for(size_t idx = 0; primes_object.get_nth_prime(idx) < 5; ++idx){
    cout << primes_object.get_nth_prime(idx);
  }
  cout << endl;

  for(size_t i = 0; i <= 5; ++i){
    if(primes_object.is_prime(i)){
      cout << i << " is prime." << endl;
    }else{
      cout << i << " is not prime." << endl;
    }
  }
}

void iterator_use_example(){
  primes primes_object(10);

  cout << "Primes: ";
  for(auto itr = primes_object.begin(); distance(primes_object.begin(), itr) < 5; ++itr)
    cout << *itr << endl;
}

void iterator_infinite_loop_example(){
  primes primes_object;

  cout << "Primes: ";
  int breaker = 0;
  for(auto itr = primes_object.begin(); itr != primes_object.end(); ++itr){
     cout << *itr << " ";

    if(breaker++ >= 5)
      break;
  }
}


void generator_example(){
  array<typename madlib::prime_type, 5> arr;

  generate(arr.begin(), arr.end(), prime_generator());

  cout << "Primes: ";
  for(auto val : arr)
    cout << val << " ";
}

int main(){
  direct_use_example();
  iterator_use_example();
  iterator_infinite_loop_example();
  generator_example();

  return 0;
}