#pragma once

#include <algorithm>
#include <concepts>
#include <type_traits>

using std::integral;
using std::signed_integral;
using std::unsigned_integral;

// For some reason, g++ 12 has constexpr std::forward_list but nothing else
// does.  This is to allow for greater compatability with everything.
#if __cplusplus > 202002L
#define PRIMES_CONSTEXPR constexpr
#else
#define PRIMES_CONSTEXPR
#endif

/*! \brief C++20 Header only prime generation class and related iterator.
 * Meant as an exercise.
 *
 * Using C++20 features and uses the Euler's Sieve
 * algorithm, this code is intended to be a usable, high quality example for
 * both C++20 usage and the associated prime algorithms.
 */

namespace madlib {

/*! Since primes are always positive, and the design of this code should store
 * all primes from start to machine memory limit, size_t is the natural choice
 * to keep the code portable and reasonably optimal even on much smaller word
 * size ISAs.
 */
using prime_type = size_t;

// Member declarations

/*! \brief the interface that all conforming implementations should conform to
 * via CRTP.
 */
template <class PRIMES_IMPLEMENTATION> class primes_interface {

public:
  /// Primes' data type for primes, defined as madlib::prime_type .
  using value_type = prime_type;

  /// Primes' reference data type for primes, defined as madlib::prime_type .
  using reference = prime_type &;

  /// Primes' constant reference data type for primes, defined as
  /// madlib::prime_type .
  using const_reference = const prime_type &;

  /// Primes' type used to convey the maximum index that can be referred to.
  using size_type = size_t;

protected:
  PRIMES_CONSTEXPR primes_interface &_interface_calculate_primes_through(
      const unsigned_integral auto &through_value);

  PRIMES_CONSTEXPR bool
  _interface_is_prime(const unsigned_integral auto &number);

  PRIMES_CONSTEXPR prime_type
  _interface_get_nth_prime(const unsigned_integral auto &n);

  PRIMES_CONSTEXPR void _interface_auto_expand_mapped_primes();

  PRIMES_CONSTEXPR void _interface_map_next_prime();

  PRIMES_CONSTEXPR size_t _interface_number_of_mapped_primes();

  PRIMES_CONSTEXPR prime_type _interface_highest_odd_number_mapped();
};

/// Support `calculate_primes_through()` by implementing
/// `calculate_primes_through_impl()`.
template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_interface<PRIMES_IMPLEMENTATION> &
primes_interface<PRIMES_IMPLEMENTATION>::_interface_calculate_primes_through(
    const unsigned_integral auto &through_value) {
  return static_cast<PRIMES_IMPLEMENTATION *>(this)
      ->calculate_primes_through_impl(through_value);
}

/// Support `is_prime()` by implementing `is_prime_impl()`.
template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR bool
primes_interface<PRIMES_IMPLEMENTATION>::_interface_is_prime(
    const unsigned_integral auto &number) {
  return static_cast<PRIMES_IMPLEMENTATION *>(this)->is_prime_impl(number);
}

/// Support `get_nth_prime()` by implementing `get_nth_prime_impl()`.
template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR prime_type
primes_interface<PRIMES_IMPLEMENTATION>::_interface_get_nth_prime(
    const unsigned_integral auto &n) {
  return static_cast<PRIMES_IMPLEMENTATION *>(this)->get_nth_prime_impl(n);
}

/// Support `auto_expand_mapped_primes()` by implementing
/// `auto_expand_mapped_primes_impl()`.
template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR void primes_interface<
    PRIMES_IMPLEMENTATION>::_interface_auto_expand_mapped_primes() {
  static_cast<PRIMES_IMPLEMENTATION *>(this)->auto_expand_mapped_primes_impl();
}

/// Support `auto_expand_mapped_primes()` by implementing
/// `auto_expand_mapped_primes_impl()`.
template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR void
primes_interface<PRIMES_IMPLEMENTATION>::_interface_map_next_prime() {
  static_cast<PRIMES_IMPLEMENTATION *>(this)->auto_expand_mapped_primes_impl();
}

/// Support `number_of_mapped_primes()` by implementing
/// `number_of_mapped_primes_impl()`.
template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR size_t
primes_interface<PRIMES_IMPLEMENTATION>::_interface_number_of_mapped_primes() {
  return static_cast<PRIMES_IMPLEMENTATION *>(this)
      ->number_of_mapped_primes_impl();
}

/// Support `highest_odd_number_mapped()` by implementing
/// `highest_odd_number_mapped_impl()`.
template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR prime_type primes_interface<
    PRIMES_IMPLEMENTATION>::_interface_highest_odd_number_mapped() {
  return static_cast<PRIMES_IMPLEMENTATION *>(this)
      ->highest_odd_number_mapped_impl();
}

} // namespace madlib
