#pragma once

#include "primes/euler_sieve_mult.hpp"
#include "primes/euler_sieve_sqrt.hpp"

using madlib::euler_sieve_mult;
using madlib::euler_sieve_sqrt;
using madlib::primes_interface;

#ifndef DEFAULT_PRIMES_IMPLEMENTATION
#define DEFAULT_PRIMES_IMPLEMENTATION euler_sieve_sqrt
#endif

/*! \brief C++20 Header only prime generation class and related iterator.
 * Meant as an exercise.
 *
 * Using C++20 features and uses the Euler's Sieve
 * algorithm, this code is intended to be a usable, high quality example for
 * both C++20 usage and the associated prime algorithms.
 */

namespace madlib {

// class declarations

template <typename PRIMES_IMPLEMENTATION> class primes;

template <class PRIMES_IMPLEMENTATION> class primes_iterator;

template <class PRIMES_IMPLEMENTATION> class primes_generator;

class primes_iterator_sentinel_t {};

/*! Newer practice for iterators where an 'end' isn't applicable is to use an
 * alternate constexpr sentinal type
 */
constexpr primes_iterator_sentinel_t primes_iterator_sentinel;

// class definitions

/*! \brief Iterator for primes.
 *
 * Conforms to the std::random_access_iterator concept, so primes can be
 * accessed safely in a random order.  Scanning from low to high values is
 * still suggested, as well as using primes's `.calculate_primes_through(n)`
 * function to calculate only the primes needed rather than the automatic
 * expansion logic which will be costly.
 */
template <class PRIMES_IMPLEMENTATION = DEFAULT_PRIMES_IMPLEMENTATION>
class primes_iterator {

  /*
  Since this iterator needs to index into a container which may over the
  course of its operation invalidate iterators, both the owning class and and
  the raw index it is using need to be kept.
  */
  primes<PRIMES_IMPLEMENTATION> *owning_object;
  size_t raw_idx;

public:
  /// Iterator's data type for primes, defined as madlib::prime_type .
  using value_type = typename madlib::prime_type;

  /// Type for distance between iterators, required by
  /// std::random_access_iterator .
  using difference_type = ssize_t;

  // Required by std::semi_regular, but if you use it then you are doing this
  // wrong and your code _will_ break.
  PRIMES_CONSTEXPR primes_iterator();

  /// Actual default constructor, taking the primes object to reference and
  /// defaulting to the first prime.
  explicit PRIMES_CONSTEXPR
  primes_iterator(primes<PRIMES_IMPLEMENTATION> *from);

  /// Constructor specifying the primes object to take from and which prime
  /// to start on.
  explicit PRIMES_CONSTEXPR
  primes_iterator(primes<PRIMES_IMPLEMENTATION> *from,
                  const unsigned_integral auto &initial_idx);

  /// Increment the prime `rhs` pointed by `lhs` times.
  friend PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
  operator+(const integral auto &lhs,
            const primes_iterator<PRIMES_IMPLEMENTATION> &rhs);

  /// Increment the prime `lhs` pointed by `rhs` times.
  friend PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
  operator+(const primes_iterator<PRIMES_IMPLEMENTATION> &lhs,
            const integral auto &rhs);

  /// Increment the prime `lhs` pointed in place by `rhs` times.
  friend PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> &
  operator+=(primes_iterator<PRIMES_IMPLEMENTATION> &lhs,
             const integral auto &rhs);

  /// Decrement the prime `rhs` pointed by `lhs` times.
  friend PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
  operator-(const integral auto &lhs,
            const primes_iterator<PRIMES_IMPLEMENTATION> &rhs);

  /// Decrement the prime `lhs` pointed by `rhs` times.
  friend PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
  operator-(const primes_iterator<PRIMES_IMPLEMENTATION> &lhs,
            const integral auto &rhs);

  /// Returns the distance in number of primes between one `lhs` and `rhs`.
  template <class PRIMES_IMPLEMENTATION_OTHER>
  friend PRIMES_CONSTEXPR difference_type
  operator-(const primes_iterator<PRIMES_IMPLEMENTATION> &lhs,
            const primes_iterator<PRIMES_IMPLEMENTATION_OTHER> &rhs);

  /// Increment the prime `lhs` pointed in place by `rhs` times.
  friend PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> &
  operator-=(primes_iterator<PRIMES_IMPLEMENTATION> &lhs,
             const integral auto &rhs);

  /// Cause the iterator to point to the next prime in place.
  PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> &operator++();

  /// Return an iterator pointing to the next prime number.
  PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> operator++(int);

  /// Cause the iterator to point to the previous prime in place.
  PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> &operator--();

  /// Return an iterator pointing to the previous prime number.
  PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> operator--(int);

  /// Return the prime number pointed to by this instance of the iterator.
  /// Will dynamically expand the number of primes mapped by the owning primes
  /// class as needed.
  PRIMES_CONSTEXPR value_type operator*() const;

  /// Equality operator required by std::random_access_iterator, ignores
  /// owning primes object for comparisons.
  template <class PRIMES_IMPLEMENTATION_OTHER>
  friend PRIMES_CONSTEXPR bool
  operator==(const primes_iterator<PRIMES_IMPLEMENTATION> &lhs,
             const primes_iterator<PRIMES_IMPLEMENTATION_OTHER> &rhs) {
    return lhs.raw_idx == rhs.raw_idx;
  }

  /// There are infinite primes, so use a Sentinel Iterator to represent this.
  PRIMES_CONSTEXPR bool operator==(primes_iterator_sentinel_t) { // NOLINT
    return false;
  }

  /// Generate all comparison operators.  Does not compare if the owning
  /// primes object is the same.
  template <class PRIMES_IMPLEMENTATION_OTHER>
  PRIMES_CONSTEXPR auto
  operator<=>(const primes_iterator<PRIMES_IMPLEMENTATION_OTHER> &rhs) const {
    return static_cast<long>(raw_idx) <=> static_cast<long>(rhs.raw_idx);
  }

  /// Access a prime whose index is the sum of this instance's index and the
  /// index passed.
  PRIMES_CONSTEXPR value_type operator[](const integral auto &index_from) const;
};

// Ensure at compile time that at least the declarations for the iterator are
// compliant to std::random_access_iterator.
static_assert(std::random_access_iterator<primes_iterator<euler_sieve_sqrt>>);

/*! \brief Universal interface for easy use of essential primes operations,
 * some types of optimization hints, and ease of use functionality.
 *
 */
template <typename PRIMES_IMPLEMENTATION = DEFAULT_PRIMES_IMPLEMENTATION>
class primes : public PRIMES_IMPLEMENTATION {
public:
  /// Cloned from madlib::prime_type
  using value_type = prime_type;

  /// Primes' iterator type.
  using iterator = primes_iterator<PRIMES_IMPLEMENTATION>;

  /// Primes' constant iterator type.
  using const_iterator = const iterator;

  /// Primes' difference type used for distance between primes.  Defined as
  /// primes_iterator::difference_type .
  using difference_type =
      typename primes_iterator<PRIMES_IMPLEMENTATION>::difference_type;

  /// Just default initialize members.
  PRIMES_CONSTEXPR primes() : PRIMES_IMPLEMENTATION() {}

  /// Construct primes object pre-calculating at least through
  /// `initially_through`.
  explicit PRIMES_CONSTEXPR
  primes(const unsigned_integral auto &initially_through);

  /// Construct primes object pre-calculating at least through
  /// `initially_through`.
  explicit PRIMES_CONSTEXPR
  primes(const signed_integral auto &initially_through);

  /// Ensure primes in [0,through_value) are calculated.  This can allow
  /// implementations to work more efficiently.
  PRIMES_CONSTEXPR primes<PRIMES_IMPLEMENTATION> &
  calculate_primes_through(const unsigned_integral auto &through_value);

  /// Ensure primes in [0,through_value) are calculated.  This can allow
  /// implementations to work more efficiently.
  PRIMES_CONSTEXPR primes<PRIMES_IMPLEMENTATION> &
  calculate_primes_through(const signed_integral auto &through_value);

  /// Derermine if `number` is prime or not.
  PRIMES_CONSTEXPR bool is_prime(const unsigned_integral auto &number);

  /// Derermine if `number` is prime or not.
  PRIMES_CONSTEXPR bool is_prime(const signed_integral auto &number);

  /// Retrieve the nth prime, expanding mapped primes if needed.
  PRIMES_CONSTEXPR prime_type get_nth_prime(const unsigned_integral auto &n);

  /// Implementation defined behavior, but it is a hint to map more primes.
  PRIMES_CONSTEXPR void auto_expand_mapped_primes();

  /// Maps the next prime known.  Characteristics are implemementation defined.
  PRIMES_CONSTEXPR void map_next_prime();

  /// Returns the number of primes known currently by the implementation
  /// instance.
  PRIMES_CONSTEXPR size_t number_of_mapped_primes();

  /// Returns the highest odd number mapped in the search for primes.
  PRIMES_CONSTEXPR prime_type highest_odd_number_mapped();

  /// Iterator pointing to the first prime.
  PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> begin();

  /// Constant iterator pointing to the first prime.
  PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> cbegin() const;

  /// Special end sentinel to represent the never reachable end of primes.
  static constexpr primes_iterator_sentinel_t end() {
    return primes_iterator_sentinel;
  }
};

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes<PRIMES_IMPLEMENTATION>::primes(
    const unsigned_integral auto &initially_through)
    : PRIMES_IMPLEMENTATION(initially_through) {}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes<PRIMES_IMPLEMENTATION>::primes(
    const signed_integral auto &initially_through)
    : PRIMES_IMPLEMENTATION(initially_through > 0
                                ? static_cast<prime_type>(initially_through)
                                : 0) {}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes<PRIMES_IMPLEMENTATION> &
primes<PRIMES_IMPLEMENTATION>::calculate_primes_through(
    const unsigned_integral auto &through_value) {
  return static_cast<primes<PRIMES_IMPLEMENTATION> &>(
      primes_interface<PRIMES_IMPLEMENTATION>::
          _interface_calculate_primes_through(through_value));
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes<PRIMES_IMPLEMENTATION> &
primes<PRIMES_IMPLEMENTATION>::calculate_primes_through(
    const signed_integral auto &through_value) {
  if (through_value < 1) {
    return static_cast<primes<PRIMES_IMPLEMENTATION> &>(*this);
  }
  return calculate_primes_through(static_cast<prime_type>(through_value));
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR bool
primes<PRIMES_IMPLEMENTATION>::is_prime(const unsigned_integral auto &number) {
  return primes_interface<PRIMES_IMPLEMENTATION>::_interface_is_prime(number);
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR bool
primes<PRIMES_IMPLEMENTATION>::is_prime(const signed_integral auto &number) {
  if (number < 2) {
    return false;
  }
  return is_prime(static_cast<prime_type>(number));
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR prime_type
primes<PRIMES_IMPLEMENTATION>::get_nth_prime(const unsigned_integral auto &n) {
  return primes_interface<PRIMES_IMPLEMENTATION>::_interface_get_nth_prime(n);
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR void
primes<PRIMES_IMPLEMENTATION>::auto_expand_mapped_primes() {
  primes_interface<
      PRIMES_IMPLEMENTATION>::_interface_auto_expand_mapped_primes();
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR void primes<PRIMES_IMPLEMENTATION>::map_next_prime() {
  primes_interface<PRIMES_IMPLEMENTATION>::_interface_map_next_prime();
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR size_t
primes<PRIMES_IMPLEMENTATION>::number_of_mapped_primes() {
  return primes_interface<
      PRIMES_IMPLEMENTATION>::_interface_number_of_mapped_primes();
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR prime_type
primes<PRIMES_IMPLEMENTATION>::highest_odd_number_mapped() {
  return primes_interface<
      PRIMES_IMPLEMENTATION>::_interface_highest_odd_number_mapped();
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
primes<PRIMES_IMPLEMENTATION>::begin() {
  return primes_iterator<PRIMES_IMPLEMENTATION>(
      static_cast<primes<PRIMES_IMPLEMENTATION> *>(this));
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
primes<PRIMES_IMPLEMENTATION>::cbegin() const {
  return primes_iterator<PRIMES_IMPLEMENTATION>(
      static_cast<primes<PRIMES_IMPLEMENTATION> *>(this));
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR
primes_iterator<PRIMES_IMPLEMENTATION>::primes_iterator()
    : owning_object(static_cast<primes<PRIMES_IMPLEMENTATION>>(nullptr)),
      raw_idx(0) {}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>::primes_iterator(
    primes<PRIMES_IMPLEMENTATION> *from)
    : owning_object(from), raw_idx(0) {}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>::primes_iterator(
    primes<PRIMES_IMPLEMENTATION> *from,
    const unsigned_integral auto &initial_idx)
    : owning_object(from), raw_idx(initial_idx) {}

/// \cond FALSE
// Tell Doxygen to skip considering this code.
template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
operator+(const integral auto &lhs,
          const primes_iterator<PRIMES_IMPLEMENTATION> &rhs) {
  return primes_iterator(rhs.owning_object,
                         static_cast<size_t>(lhs) + rhs.raw_idx);
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
operator+(const primes_iterator<PRIMES_IMPLEMENTATION> &lhs,
          const integral auto &rhs) {
  return primes_iterator(lhs.owning_object,
                         lhs.raw_idx + static_cast<size_t>(rhs));
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> &
operator+=(primes_iterator<PRIMES_IMPLEMENTATION> &lhs,
           const integral auto &rhs) {
  lhs.raw_idx += rhs;
  return lhs;
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
operator-(const integral auto &lhs,
          const primes_iterator<PRIMES_IMPLEMENTATION> &rhs) {
  return primes_iterator(rhs.owning_object,
                         static_cast<size_t>(lhs) - rhs.raw_idx);
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
operator-(const primes_iterator<PRIMES_IMPLEMENTATION> &lhs,
          const integral auto &rhs) {
  return primes_iterator(lhs.owning_object,
                         lhs.raw_idx + static_cast<size_t>(rhs));
}

template <class PRIMES_IMPLEMENTATION_LEFT, class PRIMES_IMPLEMENTATION_RIGHT>
inline PRIMES_CONSTEXPR ssize_t
operator-(const primes_iterator<PRIMES_IMPLEMENTATION_LEFT> &lhs,
          const primes_iterator<PRIMES_IMPLEMENTATION_RIGHT> &rhs) {
  return static_cast<ssize_t>(lhs.raw_idx - rhs.raw_idx);
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> &
operator-=(primes_iterator<PRIMES_IMPLEMENTATION> &lhs,
           const integral auto &rhs) {
  lhs.raw_idx -= rhs;
  return lhs;
}
/// \endcond

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> &
primes_iterator<PRIMES_IMPLEMENTATION>::operator++() {
  ++raw_idx;

  return *this;
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
primes_iterator<PRIMES_IMPLEMENTATION>::operator++(int) {
  primes_iterator old = *this;

  ++raw_idx;

  return old;
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION> &
primes_iterator<PRIMES_IMPLEMENTATION>::operator--() {
  --raw_idx;

  return *this;
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR primes_iterator<PRIMES_IMPLEMENTATION>
primes_iterator<PRIMES_IMPLEMENTATION>::operator--(int) {
  primes_iterator old = *this;

  --raw_idx;

  return old;
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR
    typename primes_iterator<PRIMES_IMPLEMENTATION>::value_type
    primes_iterator<PRIMES_IMPLEMENTATION>::operator*() const {
  return owning_object->get_nth_prime(raw_idx);
}

template <class PRIMES_IMPLEMENTATION>
inline PRIMES_CONSTEXPR
    typename primes_iterator<PRIMES_IMPLEMENTATION>::value_type
    primes_iterator<PRIMES_IMPLEMENTATION>::operator[](
        const integral auto &index_from) const {
  // Here is a dragon.
  return owning_object->get_nth_prime(
      static_cast<size_t>(raw_idx + index_from));
}

/*! \brief Simple generator for getting primes.
 *
 * This is suboptimal, and not the suggested way to get primes.
 */
template <class PRIMES_IMPLEMENTATION = DEFAULT_PRIMES_IMPLEMENTATION>
class prime_generator {
  typename primes<PRIMES_IMPLEMENTATION>::size_type prime_idx = 0;
  primes<PRIMES_IMPLEMENTATION> prime_object;

public:
  PRIMES_CONSTEXPR typename primes<PRIMES_IMPLEMENTATION>::value_type
  operator()() {
    /// On each call to the object, return the next prime.
    return prime_object.get_nth_prime(prime_idx++);
  }
};

} // namespace madlib
