#pragma once

#include <stdbool.h>

/// This is the C compatability interface.  It offers similar functionality as
/// the C++ interface but the user cannot select a preferred implementation.

#ifdef __cplusplus
extern "C" {
#endif
/// C wrapper around the C++ primes implementation.  This can only use the
/// default primes algorithm implementation.  It keeps track of any sort of
/// memory management and current prime.
struct c_primes;

/// Wrap construction of C++ primes object.
struct c_primes *
construct_c_primes_object(unsigned long long int precalculate_through_n);

/// Wrap deconstruction of C++ primes object.
void destruct_c_primes_object(struct c_primes *primes);

/// Calculate ahead of time all primes less than `n`.  Using this often
/// allows faster operation of the backend implementation.
struct c_primes *precalculate_primes_through(struct c_primes *primes,
                                             unsigned long long int n);

/// Get the nth prime.  Will calculate needed primes if needed.
unsigned long long int get_nth_prime(struct c_primes *primes,
                                     unsigned long long int n);

/// Increment the current prime `primes` is referring to.
struct c_primes *increment_current_prime(struct c_primes *primes);

/// Decrement the current prime `primes` is referring to.
struct c_primes *decrement_current_prime(struct c_primes *primes);

/// Change the current prime `primes` refers to relative to the current
/// prime `primes` is referring to.
struct c_primes *relative_change_current_primes(struct c_primes *primes,
                                                long long int rel_offset);

/// Change the current prime `primes` refers to according to the absolute
/// indexing of primes.
struct c_primes *absolute_change_current_primes(struct c_primes *primes,
                                                unsigned long long int index);

/// Retern the current prime `primes` is referring to.
unsigned long long int get_current_prime(struct c_primes *primes);

/// Tell if `n` is prime or not.
bool is_prime(struct c_primes *primes, unsigned long long int n);
#ifdef __cplusplus
}
#endif
