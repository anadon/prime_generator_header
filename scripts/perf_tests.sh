#! /usr/bin/env sh

# Pull what implementations should be tested and how far they should be tested.
# How far they are tested is measured in powers of 2.

implementations = [primes --list-implementations | xargs]

results=[[]]

for i in range 0..$1 ; do
  for impl in implementations[@]: do
    results["$impl"]["$i"] = /usr/bin/time -v primes --implementation=euler-seieve --calculate-through pow 2 "$i"


