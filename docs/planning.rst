Prime Header's Outstanding work and planning document
=====================================================

Complete
========

Partially Complete
==================

Committed
=========
- Expand tested compilers
- - older versions of g++
- - - GCC++6
- - - GCC++7
- - - GCC++8
- - - GCC++9
- - - GCC++10
- - - GCC++11
- - - GCC++13 ?
- - clang++
- - - 3.3
- - - 3.4
- - - 5
- - msvc++
- - - VS 2013
- - - VS 2015
- - - VS 2017
- - - VS 2019


Queued
======
- Document CRTP2
- - Presentation
- - CppCon Lightening Talk?
- Check documentation
- Add .precommit checks
- - pre-commit.com
- - formatting
- - - C++
- - - Markdown
- - - reStructuredText
- - unit tests
- - - coverage reports built
- - - - https://docs.coveralls.io/gitlab
- - - - - https://github.com/eddyxu/cpp-coveralls
- - spelling
- - docs build
- Expand tested compilers
- - Look into others?
- Expand static analyzers
- - cppcheck
- - clang-tidy
- AppVeryor configuration gets fixed
- Documentation
- - Examples
- - improve Sphinx setup
- - - https://www.sphinx-doc.org/en/master/usage/extensions/extlinks.html
- - https://codereview.stackexchange.com/
- - Usage
- - Adding a new implementation
- Packaging and releases
- - Reproducible Builds compliant (meson should give this for free)
- - pkg-config
- - meson wrapdb ( https://mesonbuild.com/Wrapdb-projects.html )
- - Nix
- - Guix
- - Spack (https://computing.llnl.gov/projects/spack-hpc-package-manager)
- - apt
- -
- Submit to https://codereview.stackexchange.com/
- C interface


In Deliberation
===============
- Boost graph wrapper
- Wrapper for https://github.com/kimwalisch/primesieve
- Semantic Versioning
- - https://semver.org/
- https://g.codefresh.io/2.0/workflow-templates
- Python adapter (https://github.com/pyscaffold/pyscaffold/)

The Heap and Spurious Ideas
===========================
- Compare alternative projects
- - https://pypi.org/project/primesieve/
- - https://pypi.org/project/find-primes/
- - https://pypi.org/project/primer/
- - https://pypi.org/project/pyprimesieve/
- - https://pypi.org/project/cyprimes/
- - https://pypi.org/project/gaussianprimes/
- - https://github.com/oittaa/pseudoprimes-py
- - https://pypi.org/project/prime-sieve/
- - https://github.com/eriktews/gensafeprime/blob/master/src/primemodule.c
- - https://www.boost.org/doc/libs/1_80_0/libs/multiprecision/doc/html/boost_multiprecision/tut/primetest.html
- -
- OpenSSL integration?
- Look into statistical primality testing further.
- Add docs
- Fix docs
- Examples
- Clean up docs/conf.py
- Packaging scripts
- Badges
- Git ci hooks
- Test suite
- Test coverage
- - 100% by lines, looks like branches hidden in static_cast
- Polish docs
- Pipenv/configuration.scm/nix.flake for python and tools configuration and development
- Implement spell checking for documentation, code.
- - www.npmjs.com/package/markdown-spellcheck
- - https://metacpan.org/pod/String::Comments::Extract with hunspell
- - superuser.com/a/557808
- More clear dependencies
- - Provide links, newcomers will get confused when looking for packages like ninja.
- Provide installable releases
- precommit checks
- Signing commits
