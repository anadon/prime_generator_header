.. Prime Generator Header documentation master file, created by
   sphinx-quickstart on Wed Oct 26 00:37:42 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Prime Header's Documentation
==================================================

Caution: This is verion 0 and rapidly developing.  More tests are always being added, but expect breakages until version 1.
===========================================================================================================================

.. image:: https://bestpractices.coreinfrastructure.org/projects/6617/badge.svg
    :target: https://bestpractices.coreinfrastructure.org/projects/6617
    :alt: CII Best Practices Compliance

.. image:: https://app.travis-ci.com/anadon/prime_generator_header.svg?branch=master
    :target: https://app.travis-ci.com/gitlab/anadon/prime_generator_header
    :alt: TravisCI build status

.. image:: https://ci.appveyor.com/api/projects/status/l0xca2glaewwdarx?svg=true
    :target: https://ci.appveyor.com/project/anadon/prime-generator-header
    :alt: Appveyor Build Status

.. image:: https://readthedocs.org/projects/prime-generator-header/badge/?version=latest
    :target: https://prime-generator-header.readthedocs.io/en/latest/?badge=latest
    :alt: Read the Docs build Status

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api/library_root
   planning


`For the git repository, click here <https://gitlab.com/anadon/prime_generator_header>`_

Project Goals
==================

This project has been developed to provide a reasonably complete set of mechanisms to get and use primes in C++ projects.  Additional work has been and will be made to make it an exemplar of

- Code documentation
- Making documentation accessible in multiple forms
- Packaging
- Examples
- Test suite
- CI support

And anything else to make this project the best it can possibly be in hopes of making a reference and standard from which other project (mine at least) can copy from and be held to.

Docs
====

Particular documentation on components may be browsed in the :ref:`genindex`.

Installing
==========

Dependencies only need to be installed from one source.  However, many systems provide them from multiple sources.  Depending on your system, one source may be better than another.

System Only Dependencies:

- ninja
- G++>=12
- Python>=3.7
- Doxygen
- doctest-dev
- pkgconfig

Python Dependencies or System Dependencies

- meson
- sphinx
- breathe
- exhale
- compile-time-printer


.. code-block:: sh
   :caption: Install on Ubuntu, Debian, Pop_OS

   sudo apt install ninja-build g++-12 doxygen python3 pkgconf doctest-dev
   pip install -r docs/requirements.txt

Testing

::

   meson setup build
   meson test -C  build

Installing

::

   meson setup build
   meson install -C build


Details:
At time of writing, the C++ Standards Committee has not approved P1929r0, which allows for some important components to be made `constexpr`.  The only compiler tested to work with this is GCC 12.  It is expected that the needed conponents will be made `constexpr` in C++23.  This code requires a C++20 compiler which has also adopted an implementation conforming to P1929r0.  The Python module `compile-time-printer` also requires GCC, further limiting compiler support.  This situation will improve in the future.

Examples
========

.. code-block:: cpp
   :caption: Include and Use

   #include <primes.hpp>

   using madlib::primes;
   using madlib::prime_generator;

.. code-block:: cpp
   :caption: Suggested Direct Use

   void direct_use_example(){
     primes primes_object(5);

     primes_object.calculate_primes_through(static_cast<unsigned int>(11));

     cout << "Primes: "
     for(size_t idx = 0; primes_object.get_nth_prime(idx) < 5; ++idx){
       cout << primes_object.get_nth_prime(idx);
     }
     cout << endl;

     for(size_t i = 0; i <= 5; ++i){
       if(primes_object.is_prime(i)){
         cout << i << " is prime." << endl;
       }else{
         cout << i << " is not prime." << endl;
       }
     }
   }

::

   Primes: 2 3 5 7 11
   0 is not prime.
   1 is not prime.
   2 is prime.
   3 is prime.
   4 is not prime.
   5 is prime.


Since primes don't entirely act like a typical finite container like most C++ data structures, loops should be thought of in a more guarded way, as shown below.

.. code-block:: cpp
   :caption: Suggested Iterator Loop

   void iterator_use_example(){
     primes primes_object(10);

     cout << "Primes: "
     for(auto itr = primes_object.begin(); distance(primes_object.begin(), itr) < 5; ++itr)
       cout << *itr << endl;
   }

::

   Primes: 2 3 5 7 11


Be careful, as this is actually an infinite loop unless guarded.

.. code-block:: cpp
   :caption: Typical Iterator Loop

   void iterator_infinite_loop_example(){
     primes primes_object;

     cout << "Primes: ";
     int breaker = 0;
     for(auto itr = primes_object.begin(); itr != primes_object.end(); ++itr){
        << *itr << " ";

       if(breaker++ >= 5)
         break;
     }
   }

::

   Primes: 2 3 5 7 11



Use wisely, this may overcalculate primes to the detriment of speed and memory usage.

.. code-block:: cpp
   :caption: Generator Pattern

   void generator_example(){
     array<primes::value_type, 5> arr;

     generate(arr.begin(), arr.end(), prime_generator());

     cout << "Primes: "
     for(auto val : arr)
       cout << val << " ";
   }

::

   Primes: 2 3 5 7 11



Work needed for this project
============================

This project has not reached version 1.  It needs significant work in order to reach this mile stone.  The following are general topics in need of work.  For more details, :doc:`Needed Work </planning>`.
