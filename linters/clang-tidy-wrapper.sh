#! /usr/bin/env sh

clang-tidy -extra-arg=-std=c17 include/primes.h

clang-tidy -extra-arg=-std=c++20 include/primes/primes_interface.hpp

clang-tidy -extra-arg=-std=c++20 include/primes/primes_interface.hpp include/primes/euler_sieve_sqrt.hpp

clang-tidy -extra-arg=-std=c++20 include/primes/primes_interface.hpp include/primes/euler_sieve_mult.hpp

clang-tidy -extra-arg=-std=c++20 include/primes/primes_interface.hpp include/primes/euler_sieve_sqrt.hpp include/primes/euler_sieve_mult.hpp include/primes.hpp

clang-tidy -extra-arg=-std=c++20 include/primes/primes_interface.hpp include/primes/euler_sieve_sqrt.hpp include/primes/euler_sieve_mult.hpp include/primes.h include/primes.hpp src/primes.cpp

clang-tidy -extra-arg=-std=c++20 include/primes/primes_interface.hpp include/primes/euler_sieve_sqrt.hpp include/primes/euler_sieve_mult.hpp include/primes.hpp src/cli.in.cpp
